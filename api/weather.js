import axios from "axios";

export class WeatherAPI {
  static async fetchWeatherByCoords(coords) {
    return (
      await axios.get(
        `https://api.open-meteo.com/v1/forecast?latitude=${coords.lat}&longitude=${coords.lng}&daily=weathercode,temperature_2m_max,sunrise,sunset,windspeed_10m_max&timezone=auto&current_weather=true`
      )
    ).data;
  }

  static async fetchCityByCoords(coords) {
    const response = await axios.get(
      `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${coords.lat}&lon=${coords.lng}`
    );
    const {
      address: { city, village, town },
    } = response.data;
    return city || village || town;
  }
  static async fetchCoordsByCity(city) {
    const response = await axios.get(
      `https://geocoding-api.open-meteo.com/v1/search?name=${city}&count=1&language=en&format=json`
    );
    console.log("Geocoding API response:", response.data);

    if (response.data.results.length > 0) {
      const { latitude: lat, longitude: lng } = response.data.results[0];
      return { lat, lng };
    } else {
      throw new Error("No results found for the specified city.");
    }
  }
}
