import { StyleSheet, ImageBackground, Alert } from "react-native";
import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";
import { Home } from "./pages/Home";
import { Forecasts } from "./pages/Forecasts";
import { useEffect, useState } from "react";
import {
  requestForegroundPermissionsAsync,
  getCurrentPositionAsync,
} from "expo-location";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { WeatherAPI } from "./api/weather";
import backgroundImg from "./assets/background.png";
import { useFonts } from "expo-font";
export default function App() {
  const [coordinates, setCoordinates] = useState();
  const [weather, setWeather] = useState();
  const [city, setCity] = useState();
  const [text, setText] = useState("");
  const [isFontLoaded] = useFonts({
    "Alata-Regular": require("./assets/fonts/Alata-Regular.ttf"),
  });

  console.log(text);
  const Stack = createNativeStackNavigator();
  const navTheme = {
    colors: {
      background: "transparent",
    },
  };

  const handleTextChange = (inputText) => {
    setText(inputText);
    console.log(inputText);
  };

  useEffect(() => {
    getUserCoordinates();
  }, []);

  useEffect(() => {
    if (coordinates) {
      fetchWeatherByCoords(coordinates);
      fetchCityByCoords(coordinates);
    }
  }, [coordinates]);

  const fetchWeatherByCoords = async (coords) => {
    try {
      const weatherResponse = await WeatherAPI.fetchWeatherByCoords(coords);
      setWeather(weatherResponse);
    } catch (e) {
      console.log(e.meassage);
    }
  };

  const fetchCityByCoords = async (coords) => {
    try {
      const cityResponse = await WeatherAPI.fetchCityByCoords(coords);
      setCity(cityResponse);
    } catch (e) {
      console.log(e.message);
    }
  };

  const fetchCoordsByCity = async (city) => {
    try {
      if (text) {
        const coordsResponse = await WeatherAPI.fetchCoordsByCity(city);
        setCoordinates(coordsResponse);
      } else {
        Alert.alert("Woops!");
      }
    } catch (e) {
      Alert.alert("Please provide valid credentials!");
    }
  };

  const getUserCoordinates = async () => {
    try {
      const { status } = await requestForegroundPermissionsAsync();
      if (status === "granted") {
        const location = await getCurrentPositionAsync();
        setCoordinates({
          lat: location.coords.latitude,
          lng: location.coords.longitude,
        });
      } else {
        setCoordinates({ lat: "36.54", lng: "30.41" });
      }
    } catch (e) {
      console.log(e.message);
    }
  };

  return (
    <NavigationContainer theme={navTheme}>
      <ImageBackground
        imageStyle={styles.img}
        source={backgroundImg}
        style={styles.imageBackground}
      >
        <SafeAreaProvider>
          <SafeAreaView style={styles.container}>
            {isFontLoaded && weather && (
              <Stack.Navigator
                screenOptions={{
                  headerShown: false,
                  animation: "fade",
                }}
                initialRouteName="Home"
              >
                <Stack.Screen name="Home">
                  {() => (
                    <Home
                      city={city}
                      weather={weather}
                      onSubmitSearch={fetchCoordsByCity}
                      changeHandler={handleTextChange}
                    />
                  )}
                </Stack.Screen>
                <Stack.Screen name="Forecasts">
                  {() => <Forecasts />}
                </Stack.Screen>
              </Stack.Navigator>
            )}
          </SafeAreaView>
        </SafeAreaProvider>
      </ImageBackground>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  imageBackground: {
    flex: 1,
    backgroundColor: "black",
  },
  img: {
    opacity: 0.75,
  },
});
