import { styles } from "./styles";
import { View } from "react-native";
import { WeatherBasic } from "../../components/WeatherBasic";
import { getWeatherCondition } from "../../utils/weather-util";
import { Layout } from "../../components/Layout";
import { Searchbar } from "../../components/Searchbar";
export const Home = ({ weather, city, onSubmitSearch, changeHandler }) => {
  const currentWeather = weather.current_weather;
  const currentCondition = getWeatherCondition(currentWeather.weathercode);
  return (
    <>
      <View style={styles.basic}>
        <WeatherBasic
          dailyWeather={weather.daily}
          city={city}
          temperature={Math.round(currentWeather.temperature)}
          condition={currentCondition}
        />
      </View>
      <View style={styles.searchbar}>
        <Searchbar onSubmit={onSubmitSearch} onChange={changeHandler} />
      </View>
      <View style={styles.advanced}>
        <Layout
          sunrise={weather.daily.sunrise[0].split("T")[1]}
          sunset={weather.daily.sunset[0].split("T")[1]}
          windspeed={currentWeather.windspeed}
        />
      </View>
    </>
  );
};
