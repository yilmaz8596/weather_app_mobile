import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  basic: {
    flex: 2,
  },
  searchbar: {
    flex: 2,
    marginLeft: 20,
    marginTop: 200,
  },
  advanced: {
    flex: 1,
    padding: 15,
    marginBottom: 40,
  },
});
