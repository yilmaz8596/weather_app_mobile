import { useRoute } from "@react-navigation/native";
import { Header } from "../../components/Header";
import { ListItem } from "../../components/ListItem";
import { getWeatherCondition } from "../../utils/weather-util";
export const Forecasts = () => {
  const { params } = useRoute();
  const { time } = params;

  const dayNames = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];

  const getMonthDay = (index) => {
    let month = new Date(time[index]).getMonth();
    month = month + 1;
    let day = new Date(time[index]).getDate();
    return month.toString().padStart(2, "0") + "/" + day;
  };

  const getDay = (index) => {
    return dayNames[new Date(time[index]).getDay()];
  };

  return (
    <>
      <Header city={params.city} />
      {params.time.map((item, index) => {
        const weatherCode = params.weathercode[index];
        const image = getWeatherCondition(weatherCode).image;
        const temperature = Math.round(params.temperature_2m_max[index]);
        return (
          <ListItem
            key={item}
            image={image}
            day={getDay(index)}
            date={getMonthDay(index)}
            temp={temperature}
          />
        );
      })}
    </>
  );
};
