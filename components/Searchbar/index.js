import { Alert, TextInput } from "react-native";
import { styles } from "./styles";

export const Searchbar = ({ onSubmit, onChange }) => {
  return (
    <TextInput
      placeholder="Type a city... ex: Toronto"
      style={styles.input}
      onSubmitEditing={(e) => onSubmit(e.nativeEvent.text)}
      onChangeText={onChange}
    />
  );
};
