import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
  day: {
    fontSize: 20,
  },
  date: {
    fontSize: 20,
  },
  temp: {
    minWidth: 50,
    textAlign: "right",
  },
  img: {
    width: 40,
    height: 40,
  },
});
