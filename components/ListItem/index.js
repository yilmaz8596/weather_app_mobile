import { styles } from "./styles";
import { View, Image } from "react-native";
import { Txt } from "../Txt";
export const ListItem = ({ image, day, date, temp }) => {
  return (
    <View style={styles.container}>
      <Image source={image} style={styles.img} />
      <Txt style={styles.day}>{day}</Txt>
      <Txt style={styles.date}>{date}</Txt>
      <Txt style={styles.temp}>{temp}°</Txt>
    </View>
  );
};
