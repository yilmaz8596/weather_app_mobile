import { useEffect, useState } from "react";
import { styles } from "./styles";
import { Txt } from "../Txt";

const clock = () => {
  let hours = new Date().getHours();
  let minutes = new Date().getMinutes();

  return `${hours}:${
    minutes.toString().includes("0")
      ? minutes
      : minutes.toString().padStart(2, "0")
  }`.toString();
};

export const Clock = () => {
  const [time, setTime] = useState(clock());

  useEffect(() => {
    const intId = setInterval(() => {
      setTime(clock());
    }, 1000);
    return () => {
      clearInterval(intId);
    };
  }, []);

  return (
    <>
      <Txt style={styles.time}>{time.padStart(2, "0")}</Txt>
    </>
  );
};
