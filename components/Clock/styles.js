import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  time: {
    fontSize: 15,
    marginRight: 25,
    marginTop: 25,
  },
});
