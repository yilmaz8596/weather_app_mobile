import { Text } from "react-native";
import { styles } from "./styles";

export const Txt = ({ children, style, onPress, ...props }) => {
  return (
    <Text style={[styles.text, style]} onPress={onPress} {...props}>
      {children}
    </Text>
  );
};
