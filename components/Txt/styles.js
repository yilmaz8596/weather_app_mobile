import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    color: "white",
    fontFamily: "Alata-Regular",
  },
});
