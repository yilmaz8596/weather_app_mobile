import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginBottom: 50,
  },
  backBtn: {
    width: 30,
    marginLeft: 10,
  },
  headerText: {
    flex: 1,
    marginRight: 30,
    alignItems: "center",
  },
  subtitle: {
    fontSize: 20,
  },
});
