import { styles } from "./styles";
import { TouchableOpacity, View } from "react-native";
import { Txt } from "../Txt";
import { useNavigation } from "@react-navigation/native";
export const Header = ({ city }) => {
  const nav = useNavigation();
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => nav.goBack()} style={styles.backBtn}>
        <Txt>{"<"}</Txt>
      </TouchableOpacity>
      <View style={styles.headerText}>
        <Txt>{city}</Txt>
        <Txt style={styles.subtitle}>7 day forecasts</Txt>
      </View>
    </View>
  );
};
