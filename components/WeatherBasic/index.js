import { Txt } from "../Txt";
import { styles } from "./styles";
import { View, Image, TouchableOpacity } from "react-native";
import { Clock } from "../Clock";
import { useNavigation } from "@react-navigation/native";
export const WeatherBasic = ({
  temperature,
  condition,
  city,
  dailyWeather,
}) => {
  const nav = useNavigation();
  return (
    <>
      <View style={styles.clock}>
        <Clock />
      </View>
      <View style={styles.city}>
        <Txt>{city}</Txt>
      </View>
      <View style={styles.condition}>
        <Txt style={styles.conditionTxt}>{condition.label}</Txt>
      </View>
      <View style={styles.tempContainer}>
        <TouchableOpacity
          onPress={() => nav.navigate("Forecasts", { city, ...dailyWeather })}
        >
          <Txt style={styles.temp}>{temperature}°</Txt>
        </TouchableOpacity>
        <Image style={styles.image} source={condition.image} />
      </View>
    </>
  );
};
