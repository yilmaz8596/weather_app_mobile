import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
  clock: {
    alignItems: "flex-end",
  },
  city: {
    marginLeft: 20,
  },
  condition: {
    alignSelf: "flex-end",
    transform: [{ rotate: "-90deg" }],
    marginRight: 20,
  },
  conditionTxt: {
    fontSize: 20,
  },
  tempContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "baseline",
  },
  temp: {
    fontSize: 150,
    marginLeft: 10,
  },
  image: {
    height: 90,
    width: 90,
  },
});
